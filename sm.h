#ifndef __SM__
#define __SM__

#include <stdlib.h>

#ifndef __SM_MAX_NSTATES__
#define __SM_MAX_NSTATES__ 10
#endif

#define SM_callback(T,N) T (*N)()

typedef enum {
  SM_false=0,
  SM_true
} SM_bool;

typedef struct {
  SM_callback(void, action);
  SM_callback(SM_bool, shouldExit);
} SM_state;

typedef struct {
  SM_state *states[__SM_MAX_NSTATES__];
  unsigned int index;
  unsigned int nStates;
  SM_bool isRunning;
  SM_callback(SM_bool, onEnd);
  SM_callback(void, onStart);
} SM;

SM *SM_new();
SM_state *SM_state_new(SM_callback(void, action), SM_callback(SM_bool, shouldExit));

void SM_onStart(SM *sm, SM_callback(void, onStart));
void SM_onEnd(SM *sm, SM_callback(SM_bool, onEnd));

void SM_start(SM *sm);
void SM_step(SM *sm);
void SM_next(SM *sm);
void SM_consume(SM *sm);

SM_bool SM_addState(SM *sm, SM_state *state);

#endif