#include <stdio.h>
#include <stdio.h>

#include "sm.h"

SM *SM_new() {
  SM *sm = (SM *) malloc(sizeof(SM));
  sm->nStates = 0;
  sm->isRunning = SM_false;
  sm->onEnd = NULL;
  sm->onStart = NULL;
  return sm; 
}

SM_state *SM_state_new(SM_callback(void, action), SM_callback(SM_bool, shouldExit)) {
  SM_state *state = (SM_state *) malloc(sizeof(SM_state));
  state->action = action;
  state->shouldExit = shouldExit;
  return state;
}

void SM_onEnd(SM *sm, SM_callback(SM_bool, onEnd)) {
  sm->onEnd = onEnd;
}

void SM_onStart(SM *sm, SM_callback(void, onStart)) {
  sm->onStart = onStart;
}

void SM_start(SM *sm) {
  if (sm->onStart) (sm->onStart)();
  sm->index = 0;
  sm->isRunning = SM_true;
}

void SM_next(SM *sm) {
  sm->index += 1;
}

void SM_step(SM *sm) {
  if (sm->index >= sm->nStates) {
    SM_bool shouldLoop = SM_false;
    if (sm->onEnd) shouldLoop = (sm->onEnd)();
    if (shouldLoop) sm->index = 0;
    else sm->isRunning = SM_false;
  }
  if (sm->isRunning) {
    SM_bool shoudlExit = (sm->states[sm->index]->shouldExit)();
    if (shoudlExit) SM_next(sm);
    else (sm->states[sm->index]->action)();
  }
}

SM_bool SM_addState(SM *sm, SM_state *state) {
  SM_bool returnCode = SM_true;
  if (__SM_MAX_NSTATES__ > sm->nStates) {
    sm->states[sm->nStates] = state;
    sm->nStates += 1;
    returnCode = SM_false;
  }
  return returnCode;
}

void SM_consume(SM *sm) {
  while (sm->isRunning) {
    SM_step(sm);
  }
};