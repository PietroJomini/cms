# MS Docs

+ States logic
+ Structs
+ SM_callback
+ Methods



## States logic

![](./state.png)



## Structs

+ `SM_bool`
  + `boolean logic` wrapper
  + @elements:
    + `SM_false`
    + `SM_true`
+ `SM`
  + Represent an SM
+ `SM_state`
  + Represent an SM_state



## SM_callback

Wrapper around function declaration, to facilitate passing functions as parameters

```
SM_callback(T, N) -> T (*N)()
SM_callback(void, callback) -> void (*callback)()
```



## Methods

+ `SM_new `

  + Create a new SM machine
  + @params: `void`
  + @returns: `SM *`
+ `SM_state_new`

  + Create a new SM state
  + @params: 
    + `action `:`SM_callback(void, N)`
    + `shouldexit`:`SM_callback(SM_bool, N)`
  + @returns: `SM_state *`
+ `SM_onStart`

  + Define `onStart` behavior
  + @params:
    + `sm`:`SM *`
    + `onStart`:`SM_callback(void, N)`
  + @returns: `void`
+ `SM_onEnd`

  + Define `onEnd` behavior
  + @params:
    + `sm`:`SM *`
    + `onEnd:`SM_callback(SM_bool, N)`
      + SM loop if onEnd()
  + @returns: `void`
+ `SM_start`

  + Init an SM
  + @params:
    + `sm`:`SM *`
  + @returns: `void`
+ `SM_step`

  + Consume a step
  + @params:
    + `sm`:`SM *`
  + @returns: `void`
+ `SM_next`
+ Step forward by one state
  + @params:
    + `sm`:`SM *`
  + @returns: `void`
+ `SM_consume`

  + Consume an MS
  + @params:
    + `sm`:`SM *`
  + @returns: `void`
+ `SM_addState`
  + Add a state to an MS
  + @params:
    + `sm`:`SM *`
    + `state`:`SM_state *`
  + @returns: `void`