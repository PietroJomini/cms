# SM

> State machine facility, written in c

## Installation

```c
// In your .c file
#include "path/to/sm.h"
```

```bash
# while compiling
gcc ... path/to/sm.c ...

# eg
gcc test.c sm.c -o test
```

## Usage

Create a SM

```c
SM *sm = SM_new();
```

Add a state

```c
void action() {
    printf("OwO");
}

SM_bool shouldExit() {
    return SM_true;
}

SM_addState(sm, SM_state_new(action, shouldExit));
```

Run the machine

```c
SM_satrt(sm);
SM_consume(sm);
```

## Docs

[Docs](https://gitlab.com/PietroJomini/cms/-/blob/master/docs/DOCS.md)