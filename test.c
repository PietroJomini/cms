#include <stdlib.h>
#include <stdio.h>

#include "sm.h"

/*
  A -> B -> C
  2*A 
  3*B
  2*C
*/

int cstate = 1;

void A_action() {
  cstate += 1;
  printf("A\n");
}

SM_bool A_shouldExit() {
  return cstate > 2;
}

void B_action() {
  cstate += 1;
  printf("B\n");
}

SM_bool B_shouldExit() {
  return cstate > 5;
}

void C_action() {
  cstate += 1;
  printf("C\n");
}

SM_bool C_shouldExit() {
  return cstate > 7;
}

SM_bool onEnd() {
  printf("End\n");
  cstate = 0;
  return SM_true;
}

void onStart() {
  printf("Start\n");
}

int main() {

  SM *sm = SM_new();

  SM_onStart(sm, onStart);
  SM_onEnd(sm, onEnd);

  SM_addState(sm, SM_state_new(A_action, A_shouldExit));
  SM_addState(sm, SM_state_new(B_action, B_shouldExit));
  SM_addState(sm, SM_state_new(C_action, C_shouldExit));

  SM_start(sm);
  SM_consume(sm);

  return EXIT_SUCCESS;
}